//no1
let panjang = 4;
let lebar = 3;

const kelpp = (panjang, lebar) => {
    return 2*panjang+2*lebar
}

console.log(kelpp(panjang,lebar));


//no2
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function(){
          console.log(firstName + " " + lastName)
        }
      }
}
newFunction("William", "Imoh").fullName(); //output


//no3 (mecah 1 objek/array jadi beberapa variable)
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

// //-> ES5
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;

//-> ES6
const {firstName,lastName,address,hobby} = newObject;

console.log(firstName, lastName, address, hobby); 


//no4 (kombinasi 2 array terpisah)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// //-> ES5
// const combined = west.concat(east)

//-> ES6
const CombinedArrayES6 = [...west,...east];

console.log(CombinedArrayES6) 


//no5 (template literal)
const planet = "earth" 
const view = "glass" 

//->ES5
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
// console.log(before);

//->ES6
var afterES6 = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`
console.log(afterES6);