//no1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();

for(var i = 0 ; i < daftarHewan.length ; i++){
    console.log(daftarHewan[i]);
}

//no2

function introduce(data){
    console.log('Nama saya '+data.name+', umur saya '+data.age+' tahun, alamat saya di '+data.address+', dan saya punya hobby yaitu '+data.hobby+'!');
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

//no3

function hitung_huruf_vokal(str){
    var flag=0;
    for(var i = 0 ; i < str.length ; i++){
        if( str[i]=='a' || str[i]=='e' ||  str[i]=='i' ||  str[i]=='o' ||  str[i]=='u'){
            flag+=1;
        }
        else if(str[i]=='A' || str[i]=='E' ||  str[i]=='I' ||  str[i]=='O' ||  str[i]=='U'){
            flag+=1;
        }
        else flag+=0;
    }
    return flag;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

//no4
function hitung(angka){
    var num = -2;
    
    return num+(angka*2);
}



console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8