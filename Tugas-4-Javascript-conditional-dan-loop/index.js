//no1

var nilai = 78;

if(nilai >= 85){console.log("A");}
else if(nilai >= 75 && nilai < 85){console.log("B");}
else if(nilai >= 65 && nilai < 75){console.log("C");}
else if(nilai >= 55 && nilai < 65){console.log("D");}
else console.log("E");

//no2

var tanggal = 5;
var bulan = 8;
var tahun = 2020;

switch(bulan){
    case 1: console.log(tanggal + ' ' + 'Januari' + ' ' + tahun); break;
    case 2: console.log(tanggal + ' ' + 'Februari' + ' ' + tahun); break;
    case 3: console.log(tanggal + ' ' + 'Maret' + ' ' + tahun); break;
    case 4: console.log(tanggal + ' ' + 'April' + ' ' + tahun); break;
    case 5: console.log(tanggal + ' ' + 'Mei' + ' ' + tahun); break;
    case 6: console.log(tanggal + ' ' + 'Juni' + ' ' + tahun); break;
    case 7: console.log(tanggal + ' ' + 'July' + ' ' + tahun); break;
    case 8: console.log(tanggal + ' ' + 'Agustus' + ' ' + tahun); break;
    case 9: console.log(tanggal + ' ' + 'September' + ' ' + tahun); break;
    case 10: console.log(tanggal + ' ' + 'Oktober' + ' ' + tahun); break;
    case 11: console.log(tanggal + ' ' + 'November' + ' ' + tahun); break;
    case 12: console.log(tanggal + ' ' + 'Desember' + ' ' + tahun); break;
}

//no3

var n=3;
var tampil='';

for(var i=1 ; i<=n ; i++){
    for(var j=1 ; j<=i ; j++){
        tampil+='#';
        console.log(tampil);
    }
}

//no4
var m = 10;
var tampil1='';

for(var i=1 ; i<=m ; i++){
    
    tampil1+='=';

    if(i%3 == 1){console.log(i+' - '+'I love programming');}
    else if(i%3 == 2){console.log(i+' - '+'I love Javascript');}
    else if(i%3 == 0){console.log(i+' - '+'I love VueJs'.concat('\n'+tampil1));}
}